#!/bin/bash
# This script checks for required software

set -eu

CI=${CI:=false}
OS_NAME=$(python -mplatform | tr '[:upper:]' '[:lower:]')

if [[ $CI == true ]]; then
  echo "INFO Not installing anything on CI environment for now. Exiting with last exit status..."
  exit $?
fi

echo "Check environment: $OS_NAME"

# Verify that files are owned by current user ie. detect root permissions etc"
echo "Checking permissions..."
INVALID_PERMISSIONS=$(find . ! -user $USER -print)
if [[ $INVALID_PERMISSIONS != "" ]]; then
  echo "WARN Please change the permissions for these files:"
  echo $INVALID_PERMISSIONS
  echo "INFO Use this command: sudo chown -R \$USER $PWD"
  exit 1
fi

# Smoketest environment
if [[ $OSTYPE != darwin* ]]; then
  echo "WARN Only MacOS is supported at the moment. Exiting"
  exit 1
fi

if [[ `which git` ]]; then
  echo "OK Found git!"
else
  echo "WARN Please install git first."
  exit 1
fi

if [[ `which docker-compose` ]]; then
  echo "OK Found docker-compose!"
else
  echo "WARN Please install docker-compose first."
  exit 1
fi

#!/bin/bash
# This script configures the git server

set -eu

echo "INFO Setup Git server"
curl -X POST http://localhost:3000/install \
    --silent \
    --output /dev/null \
     -d @infrastructure/gogs/config/preseed_installer.properties

echo "INFO Create Repository server"
curl -X POST http://localhost:3000/api/v1/user/repos \
     -H 'Content-Type: application/json' \
     -u devops-training:devops-training \
     --silent \
     --output /dev/null \
     -d @infrastructure/gogs/config/create_repo.json

echo "INFO Now open the repository and add your SSH key:"
echo ""
echo "open http://localhost:3000/devops-training/continuous-delivery"
echo ""
echo "INFO Login with username 'devops-training' and password 'devops-training'."

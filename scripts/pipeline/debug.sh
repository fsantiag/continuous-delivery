#!/bin/sh
# DEBUG the current build environment
#
# @see https://docs.gocd.org/current/faq/environment_variables.html#standard-gocd-environment-variables
# @see https://wiki.jenkins.io/display/JENKINS/Building+a+software+project#Buildingasoftwareproject-belowJenkinsSetEnvironmentVariables
set -eu

env

#!/bin/sh

set -eu

echo "Setup project structure"
mkdir -p reports/

echo "Run tests..."
sleep 1
echo "Test Report" > reports/index.html

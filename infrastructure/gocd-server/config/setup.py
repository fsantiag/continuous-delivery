#!/usr/bin/env python
import os
from gomatic import *

# NOTE: Use "username:password@localhost:8153", when authentication is configured
configurator = GoCdConfigurator(HostRestClient(
    "localhost:8153",
    ssl=False
))

# Configure server properties
configurator.agent_auto_register_key = "devops"

# Register config repo(s) so the '.gocd.yaml' files from the repo are recognised
configurator.ensure_replacement_of_config_repos().ensure_config_repo(
    'http://git-repo:3000/devops-training/continuous-delivery',
    'yaml.config.plugin',
    repo_id='devops-training'
)

configurator.save_updated_config()

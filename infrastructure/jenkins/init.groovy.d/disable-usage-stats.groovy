import jenkins.model.*
import hudson.security.*

def instance = Jenkins.getInstance()

instance.setNoUsageStatistics(true)
instance.save()

#!/usr/bin/env bats

load test_helper

@test "Respects CI environment" {
  export GO_REVISION=0123456789
  export GO_PIPELINE_NAME=app_deploy_testing
  export CI=true
  run bash ./app/python/scripts/pipeline/deploy.sh
  [ "$status" -eq 0 ]
  echo "$output" | grep "INFO Update version to 01234"
  echo "$output" | grep "INFO Deploying app to testing"
  # Cleanup files
  rm -f index.html
}

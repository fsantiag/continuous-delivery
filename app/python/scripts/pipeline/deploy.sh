#!/bin/bash

set -eu

SHORT_REVISION=$(echo $GO_REVISION | cut -c1-5)
echo "INFO Update version to ${SHORT_REVISION}"
touch index.html
sed s/##VERSION##/$SHORT_REVISION/g index.html

# INFO We could use bash extension but the % signs are problematic in some CI servers e.g. TeamCity
# Thus we use awk to be on the safe side
# @see https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
COMPONENT_NAME=$(echo "$GO_PIPELINE_NAME" | awk 'BEGIN {FS="_"}{print $(1)}')
STAGE=$(echo "$GO_PIPELINE_NAME" | awk 'BEGIN {FS="_"}{print $(NF)}')

echo "INFO Deploying ${COMPONENT_NAME} to ${STAGE}"

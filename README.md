# Overview

This folder contains an example setup for GoCD and Jenkins to demonstrate their
capabilities for Continuous Integration and Continuous Delivery.

This project spins up the following services to ensure a comfortable development
environment:

1. [Git Server](https://gogs.io) based on [Gogs](https://gogs.io) to host the application and pipeline code
1. [GoCD](https://www.gocd.org/) + agent
1. [Jenkins](https://jenkins.io/)
1. [Mail Server](https://github.com/mailhog/MailHog) to debug email notifications


# File structure

    ├── infrastructure             - Infrastructure
    │   ├── gocd-agent             - GoCD Agent configuration
    │   ├── gocd-server            - GoCD Server configuration
    │   ├── gocd-server-data       - GoCD data (not under version control)
    │   ├── gogs                   - Git server configuration
    │   ├── gogs-data              - Git server data (not under version control)
    │   ├── jenkins                - Jenkins configuration
    │   ├── jenkins-data           - Jenkins data (not under version control)
    │   └── <service>.properties   - ENV variables for each service
    ├── scripts                    - Scripts for common tasks e.g. 'setup'
    │   ├── pipeline               - Scripts used in pipeline tasks
    │   └── setup                  - Scripts for setup tasks
    ├── Jenkinsfile                - Jenkins pipeline configuration file (https://jenkins.io/doc/book/pipeline/jenkinsfile/)
    ├── LICENSE                    - License of this project
    ├── README.md                  - The file you are reading
    ├── docker-compose.yml         - Setup for the docker containers running the services
    └── <pipeline-name>.gocd.yaml  - GoCD Pipeline configuration files (https://github.com/tomzo/gocd-yaml-config-plugin)


# Rational

The biggest challenge for learning practices like infrastructure automation,
DevOps or Continuous Delivery is to setup a safe environment to experiment and learn.

This project wants to lower the learning curve by providing well structured
and documented steps to get common tech stacks up and running.

For this we've chosen GoCD, Jenkins and Git because they are most common from
our experience.

The design goal of this project is to have minimal impact on the host system
but maximum learning effect on the tools.

The settings for each tool are documented in README or script files. A great
amount of time has been spent on automating the setup where it makes sense.


# Requirements

- MacOS, UNIX (tested with MacOS High Sierra, Windows 10 with shell should also work)
- docker 17.12.0-ce or newer
- docker-compose 1.14.0 or newer
- Internet connection
- 5GB free disk space
- Web browser
- Terminal
- Piece of paper

Tip: Run `bash scripts/setup/verify.sh` to verify your setup.

# Getting started

- Clone this repository and change to the project folder:
```
    git clone https://gitlab.com/devops-training/continuous-delivery
    cd continuous-delivery
```
- Run `docker-compose up` to start the services

This takes some time because docker will download the necessary images
and start the services. Once the services are up, continue with the setup below.

## Alternative Setup
You can also run the configure.sh script to setup the environment.
This script will run docker-compose and also perform all necessary configurations
for Gogs, GoCD and Jenkins.
Before you run, make sure you have a public under the following path with the
following name **~/.ssh/id_rsa**.

`bash configure.sh`

Sit down and grab a coffee, it will take a while. :)

Tip: If you want to revert the script setup, just run: `bash configure.sh --clean `

# Setup

## Configure the [Git Server](http://localhost:3000)

- Configure the server
```
    bash ./scripts/setup/git_server.sh
```
This creates a user and a repository for you.

Open [the web interface](http://localhost:3000/devops-training/continuous-delivery)
and login with these user credentials:
```
    Username: devops-training
    Password: devops-training
```

Now add your SSH key to the git server. This allows you to push code to the server.

- Make sure you have a SSH key available:
```
    file ~/.ssh/id_rsa.pub
    # should return 'OpenSSH RSA public key'
```
In case this key file is missing, you can create a new key. See FAQ below.

- Copy your SSH _public_ key:
```
    cat ~/.ssh/id_rsa.pub | pbcopy
```
- Open the [settings](http://localhost:3000/user/settings/ssh) and paste the key
  into the 'Content' field.

  You can use any "Key Name" for example "devops-training". I use the comment of
  the key itself when available, e.g. "devops-training-example-key".

## Configure the Git repository

- Add the 'local' remote so you are able to push to the local Git server:
```
    git remote add local ssh://git@localhost:10022/devops-training/continuous-delivery
```
This ensures that you can push your local changes to your local Git server and
still be able to fetch updates from the project's 'origin'.

Now push the repository to your local Git server:
```
    git push local master
```

## Configure [Jenkins](http://localhost:8080)

- Make sure the service is up and running:
```
    open http://localhost:8080
```
- Login with these credentials:
```
    Username: devops-training
    Password: devops-training
```
- Create a Pipeline job: http://localhost:8080/newJob

- Under "Pipeline > Definition" choose "Pipeline script from SCM"

- Select "SCM > Git" and add this URL as "Repository URL":
```
    http://git-repo:3000/devops-training/continuous-delivery
```
- Click "Save"

- Start the build by clicking "Build Now"

- Congratulations! The pipeline will appear during the build

## Configure [GoCD](http://localhost:8153/go) server

This setup uses [MailHog](https://github.com/mailhog/MailHog) to simulate a mail server.
This allows you to test email notifications without the need of a real webserver.

The mailserver starts automatically when you use `docker-compose up`.

- Make sure the service is up and running:
```
    open http://localhost:8153/go
```
- Execute this command to configure the server:
```
    docker-compose exec gocd-server python /tmp/config/setup.py
```
This will set basic parameters e.g. an access token to [enable build agents automatically](https://docs.gocd.org/current/advanced_usage/agent_auto_register.html).

After a while the build agent will appear in [the Agent list](http://localhost:8153/go/agents) and GoCD is ready.

## Open the Mail server (optional)

No configuration is required for this service, but it can be handy to have a
browser tab open once you've configured the services to use SMTP:

    open http://localhost:8025

### Jenkins

- Open the configuration settings http://localhost:8080/configure
- Go to "E-mail Notification"
- Add the server details (click on 'Advanced...' to enter the SMTP port):
```
    SMTP server: mailserver
    SMTP Port: 1025
```

### GoCD

The mailserver is already set up. You can send a test email here: http://localhost:8153/go/admin/config/server#mail_host_config

### Git Server

The mailserver is already set up. You can send a test email here: http://localhost:3000/admin/config


# Verify Setup

Check the following services are up before you proceed:

- [Git Server](http://localhost:3000)
- [GoCD](http://localhost:8153/go)
- [Jenkins](http://localhost:8080)
- [Mail Server](http://localhost:8025)

Tip: Open each service in a separate browser tab to have them handy at all times.


# Default settings

For your convenience the default user and password is always `devops-training`
unless stated otherwise.

The settings used in this repository are optimised for convenience and are
purposely NOT (!) suitable for production usage.


# Running tests

This repository contains tests based on [bats](https://github.com/sstephenson/bats)
e.g. to verify files. Run them as follows:

    bats test/*

The tests need the `bats` test framework. Install it with this command:

    brew install bats


# FAQ

## General

### Why is the Git URL sometimes `localhost` and sometimes `git-repo`?

Use `localhost` when calling the repo from your host machine.
Use `git-repo` when calling the repo _inside_ the docker network.

### How do I create a new SSH key?

In case the SSH key is missing, or you'd like to create a dedicated SSH key for
this training, create a new key by running the commands below.

This creates a new private key inside the `~/.ssh/` folder.

The key file itself is called `devops-training`. The public key file has a `.pub`
extension and is called 'devops-training.pub'.

For easy identification the key has a comment 'devops-training-example-key'. This
helps you to identify the key e.g. when using multiple keys in WebApps like Gogs, Github, Gitlab etc.
```
mkdir -p $HOME/.ssh/
ssh-keygen -f $HOME/.ssh/devops-training -C devops-training-example-key
```

Now copy your SSH _public_ key into your clipboard:
```
    cat ~/.ssh/devops-training.pub | pbcopy
```

More details: https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/


### I get an error when executing `git push`

This is most probably caused when by an existing key in the host file e.g. when
running the setup for a second time:

    The authenticity of host '[localhost]:10022 ([::1]:10022)' can't be established.

Remove all `localhost` entries in this file:

    $HOME/.ssh/known_hosts

Now try to push again.

### OMG there are hardcoded passwords!

Yes, that's right. The reason for this is to make the setup steps easier for you.
In some places you can replace the passwords with authentication tokens.

### How do I teardown the setup and remove all files?

- Stop and remove containers, networks, images, and volumes:
```
    docker-compose down
```
- Remove the docker images (optional).

  Before you delete them, make sure no other project depends on these images!

  List the images with `docker images` and remove each with this command:
```
    docker rmi <IMAGE ID>
```
- Finally remove the project folder which will remove all project files:
```
    rm -rf ./continuous-delivery
```

## GoCD

### How do I investigate issues on the agent?

Connect to the agent and run the shell commands e.g. `tail -f /godata/logs/*`

    docker-compose exec gocd-agent bash

### How to upload artifacts to the server?

Execute this example command:

    curl http://localhost:8153/go/files/pipeline-name/1/stage-name/1/job-name/folder-name/artifact.zip \
         -H 'Confirm:true' \
         -X POST \
         -F 'file=@dist/artifact.zip'

Add `-u username:password` if you have authentication enabled.

See https://api.gocd.org/current/#create-artifact

### How to get artifacts from the server?

    curl http://localhost:8153/go/files/pipeline-name/1/stage-name/1/job-name/folder-name/artifact.zip

Add `-u username:password` if you have authentication enabled.

### How to generate encrypted variables

Use the API as follows:

    curl http://localhost:8153/go/api/admin/encrypt \
         -H 'Accept: application/vnd.go.cd.v1+json' \
         -H 'Content-Type: application/json' \
         -X POST -d '{
           "value": "devops-training"
         }'

## I have another issue / question / idea

Ping me if you need assistance or [open a ticket in the issue tracker](/../issues). Thank you!

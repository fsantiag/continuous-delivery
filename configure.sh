#!/bin/bash
# This script will configure the environment for Gogs, GoCD and Jenkins

cancel_configuration() {
    echo "Reverting installation!"
    trap - SIGINT SIGTERM # clear the trap
    docker-compose down
    rm -rf infrastructure/jenkins-data/ infrastructure/gogs-data/ infrastructure/gocd-server-data/

    git remote remove local >/dev/null 2>&1
    sed -i '' '/\[localhost\]:10022/d' ~/.ssh/known_hosts
    exit 0
}

if [ "$1" == "--clean" ]
then
    cancel_configuration
    exit 0
fi

trap cancel_configuration SIGINT SIGTERM

wait_for_server() {
    failed=1
    while [ $failed -ne 0 ]
    do
        curl -m 1 localhost:$1 \
             -s \
             -o /dev/null
        failed=$?
        sleep 1
    done
}

docker-compose up > compose.log 2>&1 &

echo "INFO setup Git server"

echo "Waiting for Gogs startup... (http://localhost:3000)"
wait_for_server 3000

echo "-- Configuring server"
curl -X POST http://localhost:3000/install \
     -d @infrastructure/gogs/config/preseed_installer.properties \
     -o /dev/null \
     -s


echo "-- Creating repository"
curl -X POST http://localhost:3000/api/v1/user/repos \
     -H 'Content-Type: application/json' \
     -u devops-training:devops-training \
     -d @infrastructure/gogs/config/create_repo.json \
     -o /dev/null \
     -s


echo "-- Submiting public key: ~/.ssh/id_rsa.pub"
publick_key=`cat ~/.ssh/id_rsa.pub`
curl -X POST http://localhost:3000/api/v1/user/keys \
     -H 'Content-Type: application/json' \
     -u devops-training:devops-training \
     -d "{\"title\": \"my_key\", \"key\": \"$publick_key\"}" \
     -o /dev/null \
     -s


echo "-- Adding 'local' remote"
git remote add local ssh://git@localhost:10022/devops-training/continuous-delivery

echo "-- Pushing changes to 'local'"
GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no" git push -q local master

echo "Gogs setup completed"
echo ""
echo "INFO setup GoCD server"

echo "Waiting for GoCD startup... (http://localhost:8153)"
wait_for_server 8153

echo "-- Running python script to configure GoCD"
docker-compose exec gocd-server python /tmp/config/setup.py
echo "GoCD setup completed"

echo ""

echo "INFO setup Jenkins server"

echo "Waiting for Jenkins startup... (http://localhost:8080)"
wait_for_server 8080

echo "-- Waiting for Jenkins installation to finish..."
failed=1
counter=0
# Jenkins displays the authentication page when the installation
# is not completed yet. This loop makes sure we will only create
# the pipeline once the server is 100% up
while [ "$counter" -lt 10 ]
do
    curl -m 1 -s localhost:8080 | grep "Authentication required" > /dev/null
    failed=$?
    if [ "$failed" -eq 0 ]
    then
        counter=$((counter + 1))
    else
        counter=0
    fi
    sleep 2
done

echo "-- Creating pipeline based on template"
curl -X POST 'http://localhost:8080/createItem?name=pipeline' \
     -H "Content-Type:text/xml" \
     -u devops-training:devops-training \
     -d @infrastructure/jenkins/jenkins-pipeline.xml \
     -o /dev/null \
     -s

echo "Jenkins setup completed"
